## Installation

Just run `composer install [package_name]`

After that all these functions should be available in every PHP file (via composer's autoloader)

## Array related

- [where](#where)

- [randomize](#randomize)

- [pluck](#pluck)

- [not_empty](#not_empty)

- [array_map_key_values](#array_map_key_values)

- [array_map_args](#array_map_args)

- [sort_by_key](#sort_by_key)

- [group_by](#group_by)

- [chunk](#chunk)

- [array_set](#array_set)

- [array_get](#array_get)

- [json_decode_array](#json_decode_array)

- [has_keys](#has_keys)

- [pluck_keys](#pluck_keys)

- [array_flatten](#array_flatten)

- [pluck_keys_ignore_empty](#pluck_keys_ignore_empty)

- [is_hash](#is_hash)

- [is_email](#is_email)

- [is_phone](#is_phone)

- [is_url](#is_url)

- [array_first](#array_first)

- [array_last](#array_last)

- [array_filter_regex](#array_filter_regex)

- [array_except](#array_except)

- [in_array_nc](#in_array_nc)

## String related

- [lines](#lines)

- [single_line](#single_line)

- [ansi](#ansi)

- [slugify](#slugify)

- [password](#password)

- [str_insert](#str_insert)

- [str_replace_block](#str_replace_block)

- [str_wrap](#str_wrap)

- [str_quote](#str_quote)

- [str_match_all_words](#str_match_all_words)

- [str_match_brackets](#str_match_brackets)

- [next_version](#next_version)

- [kebab](#kebab)

- [camel](#camel)

- [unslugify](#unslugify)

- [remove_parens](#remove_parens)

- [template](#template)

- [file_template](#file_template)

- [bool](#bool)

- [eval_php](#eval_php)

- [words](#words)

- [sentences](#sentences)

- [split_name](#split_name)

- [stop_words](#stop_words)

- [extract_email](#extract_email)

- [email_format](#email_format)

- [html2text](#html2text)

- [cookie_get_json](#cookie_get_json)

- [cookie_set_json](#cookie_set_json)

- [self_uri](#self_uri)

- [absolute_uri](#absolute_uri)

- [url_append_params](#url_append_params)

- [file_name](#file_name)

- [file_ext](#file_ext)

- [change_ext](#change_ext)

- [file_arr](#file_arr)

## File related

- [read_csv](#read_csv)

- [read_json](#read_json)

- [fix_json](#fix_json)

- [read_config](#read_config)

- [dot_env](#dot_env)

- [write_file](#write_file)

- [write_json](#write_json)

- [jwt_encode](#jwt_encode)

- [jwt_decode](#jwt_decode)

- [unix_path](#unix_path)

- [dos_path](#dos_path)

- [relative_path](#relative_path)

- [temp_file](#temp_file)

- [image_size](#image_size)

- [placeholder_image](#placeholder_image)

- [make_thumb](#make_thumb)

- [home_dir](#home_dir)

- [directory](#directory)

- [deltree](#deltree)

- [mime_type](#mime_type)

- [basename_url](#basename_url)

## Internet related

- [curl](#curl)

- [upload](#upload)

- [upload_file](#upload_file)

- [s3_contents](#s3_contents)

- [decode_image_data](#decode_image_data)

- [gravatar](#gravatar)

- [is_disposable_email](#is_disposable_email)

- [blank_image](#blank_image)

- [imgur](#imgur)

- [dom_path](#dom_path)

- [download](#download)

- [email](#email)

- [domain](#domain)

- [fake_data](#fake_data)

- [today](#today)

- [elapsed](#elapsed)

- [mysql_date](#mysql_date)

- [is_debug](#is_debug)

- [debug](#debug)

- [warning](#warning)

- [app_dir](#app_dir)

- [log_to_file](#log_to_file)

- [log_error](#log_error)

- [ip_address](#ip_address)

- [env](#env)

- [os](#os)

- [cmd](#cmd)

- [bg](#bg)

- [cmd_capture](#cmd_capture)

- [cmd_parallel](#cmd_parallel)

- [cache_memory](#cache_memory)

- [cache_local](#cache_local)

- [confirm](#confirm)

- [dump](#dump)

- [encrypt_str](#encrypt_str)

- [decrypt_str](#decrypt_str)

- [is_cli](#is_cli)

- [get_argv](#get_argv)

- [show_errors](#show_errors)


## Array related functions

### where

Return data matching specific key value condition


 

```php
function where(array $array = [], array $cond = [])
```


Example:

```php
$match = where([['a'=>1],['b'=>2]], ['b'=>2]);
```

### randomize

Returns the random version of array (original array is unchanged)

```php
function randomize(array $array)
```


### pluck

Returns same key from all items of a multi-dimensional array

```php
function pluck(array $array, string $match)
```


### not_empty

Opposite of empty. Returns $value if not empty, otherwise false.
 Great for `if` loops. Also returns false if a hash has all empty values.




```php
function not_empty($value)
```


Example:

```php
$v = not_empty([]); // returns false
$v = not_empty(['a'=>'','b'=>'']); // returns false
$v = not_empty([1,2]); // returns [1,2]
```

### array_map_key_values

Like array map but it's possible to change both keys and values (original array is unchanged)


```php
function array_map_key_values($closure, $arr)
```


Example:

```php
$v = array_map_key_values(function (&$k, &$v) { $k = "$k-a"; $v = "$v-3"; }, ['a' => 1, 'b' => 2, 'c' => 3]);
```

### array_map_args

Calls array_map function with args


```php
function array_map_args($fn, $array, ...$args)
```


Example:

```php
$trim = array_map_args('ltrim', ['/test', '/best'], '/'); //passes '/' to ltrim
```

### sort_by_key

Sorts an array by any key


```php
function sort_by_key($arr, $key, $descending = FALSE)
```


Example:

```php
$v = sort_by_key([['name' => 'moe',   'age' => 40],['name' => 'larry', 'age' => 50],['name' => 'curly', 'age' => 60]], 'age', true);
```

### group_by

Groups an array's item by any key


```php
function group_by($arr, $match)
```


Example:

```php
$v = group_by([['name' => 'maciej',    'continent' => 'Europe'],['name' => 'yoan',      'continent' => 'Europe'],['name' => 'brandtley', 'continent' => 'North America']], 'continent');
```

### chunk

Breaks array into chunks

 // → [[1, 2, 3], [4, 5]]

```php
function chunk(array $array, $size = 1, $preserveKeys = FALSE)
```


Example:

```php
$v = chunk([1, 2, 3, 4, 5], 3);
```

### array_set

Set an array item using dot.path notation


```php
function array_set(array &$arr, $path, $val)
```


Example:

```php
$arr = []; array_set($arr, 'a.b.c.d', 11);
```

### array_get

Gets an array item using dot.path notation


```php
function array_get(array $arr, $path, $default = NULL)
```


Example:

```php
$v = array_get($arr, 'a.b.c.d', 'default');
```

### json_decode_array

Converts string, object or stdClass into an array
 ($preserve = true, leave empty objects "{}" as stdClass otherwise they convert to "[]")

```php
function json_decode_array($obj, $preserve = TRUE)
```


### has_keys

Check if the all keys exist in the array (and are not empty)

 Return value is hash with only keys

```php
function has_keys($hash, ...$keys)
```


### pluck_keys

Returns only selected keys from a hash (including empty keys)

```php
function pluck_keys($hash, ...$keys)
```


### array_flatten

Flattens a multi-dimensional array

```php
function array_flatten(array $array)
```


### pluck_keys_ignore_empty

Returns only selected keys from a hash (ignores empty values)

```php
function pluck_keys_ignore_empty($hash, ...$keys)
```


### is_hash

Check if an array is associative

```php
function is_hash(array $arr)
```


### is_email

Checks if the string is an email (returns $email if true otherwise false)

```php
function is_email($email)
```


### is_phone

Checks if the string is a phone number (returns $phone if true otherwise false)

```php
function is_phone($phone)
```


### is_url

Checks if the string is a url (returns $url if true otherwise false)

```php
function is_url($url)
```


### array_first

Gets the first item of array or hash


 @return array|string

```php
function array_first($array, $reverse = FALSE)
```


Example:

```php
$first = array_first([1,2,3]) => 1;
```

### array_last

Get the last item of array or hash


```php
function array_last($array)
```


Example:

```php
$last = array_first([1,2,3], true) => 3;
```

### array_filter_regex

Filters all array items by regex


```php
function array_filter_regex($regex, $array)
```


Example:

```php
$php = array_filter_regex('/\.php$/', $files);
```

### array_except

Removes selected items from array / or keys from hash

```php
function array_except(array $arr, ...$args)
```


### in_array_nc

Case in-sensitive in_array function

```php
function in_array_nc($needle, $haystack)
```



## String related functions

### lines

Breaks string into array


 

```php
function lines($str, $delim = '\r?\n')
```


Example:

```php
$lines = lines("this\nis\n\na\r\ntest");
```

### single_line

Convert multi-line text into a single line


```php
function single_line($text, $delim = '')
```


Example:

```php
$merge = single_line("this\nis\n\na\ntest", " - ");
```

### ansi

Remove non-ansi characters from string

```php
function ansi($str)
```


### slugify

Creates a slugified version of a word or phrase


```php
function slugify($text, $unique = FALSE)
```


Example:

```php
$unique = slugify('this is a test', true = adds a number or increments existing number);
```

### password

Generates a random password

```php
function password($length = 16)
```


### str_insert

Inserts a string after a matching phrase in an existing string

```php
function str_insert($document, $match, $insert, $ignore_case = FALSE, $replace = FALSE)
```


### str_replace_block

Replaces a block between matching tags with an existing string


```php
function str_replace_block($document, $match_start, $match_end, $insert, $ignore_case = FALSE, $replace = FALSE)
```


Example:

```php
$out = str_replace_block('data', '// ** start', '// ** end', 'new content', false, false);
```

### str_wrap

Wraps a string inside characters (return '' if string is blank) - automatically determins the right char is NULL


```php
function str_wrap($input, $left, $right = NULL, $wrap_blank = FALSE)
```


Example:

```php
$str = str_wrap('test', "<", ">", true); => <test>, $str = str_wrap('ok', '"'); => "ok", $str = str_wrap('', '<b>'); => ''
```

### str_quote

Put quotes around a string (or items of an array)


```php
function str_quote($input, $char = '"', $trim = TRUE)
```


Example:

```php
$arr = str_quote([1, 'one ', '"two"']); //["1", "one", "two"]
```

### str_match_all_words

Returns true if a string contains all the words


```php
function str_match_all_words($str, array $words, $whole_words = TRUE, $ignore_case = TRUE, $order = FALSE)
```


Example:

```php
$match = str_match_all_words('this is a test', ['/Th.s/i', 'a', 'test'], true, true, true); //order = words must be in order. Note: 1st word is regex, 2nd is a whole word.
```

### str_match_brackets

Returns all the balanced matching brackets in the string

```php
function str_match_brackets($str, $bracket = '
```


### next_version

Increase the version number by 1

```php
function next_version($version)
```


### kebab

Coverts any word into kebab-case

```php
function kebab($input, $delim = '-')
```


### camel

Coverts any word to camelCase

```php
function camel($string)
```


### unslugify

Converts text in camel/kebab/underscore case to normal text

```php
function unslugify($slug, $ucwords = FALSE)
```


### remove_parens

Returns any text inside parenthesis from a string


```php
function remove_parens($string)
```


Example:

```php
$str = remove_parens("Hustlin' ((Remix) Album Version (Explicit)) with (a(bbb(ccc)b)a) speed!");
```

### template

Replaces any tags inside template


```php
function template($str, $tags = [], $ignore_missing = FALSE)
```


Example:

```php
$out = template('hi {{first}}, today is {{date}}', ['first' => 'san', 'date' => today()]);
```

### file_template

Same as template, except it uses a file instead of string

```php
function file_template($path, $tags = [])
```


### bool

Converts a string value to it's equivanelt boolean

```php
function bool($str)
```


### eval_php

Evaluates a PHP string like include_once


```php
function eval_php($code, $vars = [])
```


Example:

```php
$result = eval_php('Hi <?php echo 1; ?>!', get_defined_vars()); //Hi 1!
```

### words

Splits a sentence in tor words


```php
function words($text)
```


Example:

```php
$v = words('this is mr.san "hello" there!');
```

### sentences

Splits a paragraph into sentences


```php
function sentences($str)
```


Example:

```php
$v = sentences('this is a test. hello dr.dre, how are you? ok, bye.');
```

### split_name

Splits a name into first and last

```php
function split_name($name, $default = 'Member')
```


### stop_words

Lists most common English stopwords

```php
function stop_words()
```


### extract_email

Extracts an email address from text

```php
function extract_email($text, $single = TRUE)
```


### email_format

Convert name, email to "name" <email>

```php
function email_format($email, $name = '')
```


### html2text

Converts html to text

```php
function html2text($html, $extract_links = TRUE)
```


### cookie_get_json

Gets the cookie value or returns $default using JSON format

```php
function cookie_get_json($name, $default = [])
```


### cookie_set_json

Sets a cookie in browser using JSON format

```php
function cookie_set_json($name, $value, $expires = '+1 day', $append = FALSE, $domain = '')
```


### self_uri

Return the (sanitized) URL of current page

```php
function self_uri($params = FALSE)
```


### absolute_uri

Appends the http part if not already present in uri

```php
function absolute_uri($path, $host = '')
```


### url_append_params

Appends new params at the end of existing URL

```php
function url_append_params($url, $params = [])
```


### file_name

Returns the name of file without extension


```php
function file_name($path)
```


Example:

```php
$name = file_name('c:/test.jpg'); //test
```

### file_ext

Returns the extension of file (without the dot)


```php
function file_ext($path)
```


Example:

```php
$ext = file_name('c:/test.jpg'); //jpg
```

### change_ext

Changes file extension (and optionally path)


```php
function change_ext($path, $new_ext, $new_path = '')
```


Example:

```php
$path = change_ext('c:/d/tmp.txt', 'mp3', [new_path]); # c:/d/tmp.mp3
```

### file_arr

Reads a file into an array


```php
function file_arr($path)
```


Example:

```php
$arr = file_arr('some.txt');
```


## File related functions

### read_csv

Reads a csv file (with header support)


 

```php
function read_csv($path, $header = FALSE, $delim = ',')
```


Example:

```php
$arr = read_csv('file', ['col1', 'col2'] | true = first line is header | false = no header);
```

### read_json

Reads a json file (optionally returning a property using dot path notation)


```php
function read_json($path, $prop = NULL, $default = NULL)
```


Example:

```php
$prop = read_json('file'[, 'package.details.name', 'untitled'\);
```

### fix_json

Converts a Javascript object into valid JSON

```php
function fix_json($string, $fixNames = TRUE, $asArray = TRUE)
```


### read_config

Parses env file into a hash

```php
function read_config($path, $name = NULL)
```


### dot_env

Reads a .env file into enviroment variables

```php
function dot_env($path)
```


### write_file

Writes to
 a file (optionally json_encoding any non-scalar data) and returns filename on success

```php
function write_file($fn, $data, $append = FALSE)
```


### write_json

Updates an existing json file setting (array of) properties using dot path notation


```php
function write_json($path, array $props, $saveAs = NULL)
```


Example:

```php
$result = write_json('file', ['package.details.name' => 'test', 'version' => 2][, 'save-as']); //true or false
```

### jwt_encode

JWT encodes a payload with $key


```php
function jwt_encode($payload, $key = '')
```


Example:

```php
$token = jwt_encode(['user_id' => 1][, 'secret' | env(APP_KEY)]);
```

### jwt_decode

JWT decodes a payload verifies it with $key


```php
function jwt_decode($jwt, $key = '')
```


Example:

```php
$result = jwt_decode('token'[, 'secret' | env(APP_KEY)]);
```

### unix_path

Converts everything to forward slashes

```php
function unix_path($path)
```


### dos_path

Converts everything to back slashes

```php
function dos_path($path)
```


### relative_path

Converts absolute path into relative path


```php
function relative_path($docRoot, $path)
```


Example:

```php
$relPath = relative_path('d:/songs', 'd:/songs/coldplay/amsterdam.txt');
```

### temp_file

Creates a temporary file with given extension and prefix

```php
function temp_file($ext = 'tmp', $prefix = 'tmp')
```


### image_size

Get [width, height] of images

```php
function image_size($path)
```


### placeholder_image

Returns a random placeholder image

```php
function placeholder_image($width = 320, $height = 200, $type = 'any')
```


### make_thumb

Creates a thumbnail from any image

```php
function make_thumb($src, $dest, $max_width = 100, $max_height = 100)
```


### home_dir

Get current users's home directory

```php
function home_dir()
```


### directory

Returns all files in the directory


```php
function directory($path, $depth = -1, $regex = NULL, $exclude = [], $sort = FALSE, $includeDirs = FALSE)
```


Example:

```php
$files = directory('c:/tmp', depth [0 = no recursion, 1 = 1 level, -1 infinite], '/\.php$/' (optional regex for basename), [..dirs to exclude]);
```

### deltree

@param SplFileInfo                     $file
 @param mixed                           $key
 @param RecursiveCallbackFilterIterator $iterator

 @return bool True if you need to recurse or if the item is acceptable

```php
function deltree($dir)
```


### mime_type

Returns mime type of any file


```php
function mime_type($fileName)
```


Example:

```php
$mime = mime_type('test.jpg');
```

### basename_url

Returns the last part of URL without query string

```php
function basename_url($url)
```



## Internet related functions

### curl

Performs a GET/POST request with $params using CURL


 

```php
function curl($url, $method = 'get', $params = [], $headers = [], &$status = NULL)
```


Example:

```php
$response = url_get('http://www.google.com/search', 'get', ['q' => 'test'] | json (post body), ['header1', 'header2'], &$status);
```

### upload

Saves data to Amazon S3

```php
function upload($data, $fileName, $bucket, $mime = '', $acl = 'public-read')
```


### upload_file

Uploads a file to Amazon S3

```php
function upload_file($file, $bucket, $path = '', $mime = '', $acl = 'public-read')
```


### s3_contents

Lists the contents of a S3 bucket


```php
function s3_contents($bucket, $path = '', $fileName = '/')
```


Example:

```php
$list = s3_contents('bucket', 'dir'); // returns all files in dir, $file_data = s3_contents('bucket', '', 'path_to_file'); // returns file's data instead
```

### decode_image_data

@var \SimpleXMLElement $tag *

```php
function decode_image_data($data, &$type = [])
```


### gravatar

Returns the gravatar url from email

```php
function gravatar($email, $null_if_missing = FALSE)
```


### is_disposable_email

Checks if the email address is disposable

```php
function is_disposable_email($email, $mx_check = TRUE)
```


### blank_image

Returns data for a blank image


```php
function blank_image($type = 'gif')
```


Example:

```php
$data = blank_image('gif|png')
```

### imgur

Uploads an image to imgur and return it's online URL

 The following env vars must be defined: IMGUR_KEY

```php
function imgur($path)
```


### dom_path

Creates a DOM document and returns it's xpath selector


  For CSS selector: Use BrowserKit or pQuery!
  More here: https://stackoverflow.com/a/260609/1031454

```php
function dom_path($html)
```


Example:

```php
$xpath = dom_path(curl('http://www.example.com')); // foreach($xpath->query('//h1') as $node) print $node->textContent;
```

### download

Downloads a file


```php
function download($url, $file = NULL, $headers = [])
```


Example:

```php
$file = download('http://www.bakalafoundation.org/wp/wp-content/uploads/2016/03/Google_logo_420_color_2x-1.png'[, 'c:/tmp/test.jpg']);
```

### email

Send out an email using Amazon SES

 The following keys must be defined: AWS_SECRET_ACCESS_KEY, AWS_ACCESS_KEY_ID

```php
function email($to, $subject, $body, $attach = NULL)
```


### domain

Gets the domain from URL with or without sub-domain

```php
function domain($url, $tld = TRUE)
```


### fake_data

Return fake user data from uinames.com


```php
function fake_data()
```


Example:

```php
$user = fake_data();
```

### today

## Other functions

 Return's today's date in dd-mm-yyyy format (utc)


```php
function today($format = 'dd-mm-yyyy', $time = NULL)
```


Example:

```php
$v = today('dd/mm/yy')
```

### elapsed

Show number of seconds elapsed after $time

```php
function elapsed($time, $now = NULL)
```


### mysql_date

Coverts any date to Mysql format

```php
function mysql_date($date = NULL)
```


### is_debug

Checks if debugging is enabled, i.e. global $debug var is set or DEBUG env is present

```php
function is_debug()
```


### debug

Prints to STDERR

```php
function debug(...$args)
```


### warning

Prints to STDERR with red color

```php
function warning(...$args)
```


### app_dir

Return app's base dir

```php
function app_dir($path = NULL, $check_exists = TRUE)
```


### log_to_file

Write a line to log file

```php
function log_to_file($file, ...$args)
```


### log_error

Logs error in ~/error.log

```php
function log_error(...$args)
```


### ip_address

Get's the local IP address

```php
function ip_address()
```


### env

Return value of an environment variable

```php
function env($name, $default = NULL)
```


### os

Return the OS name (windows, mac, or linux)

```php
function os()
```


### cmd

Runs a command using `system`
 - If any arg starts with a @ (or is not a scalar), it is saved in tmpfile and the file path is used instead
 - If first arg is "false" then the command is dry-run

 cmd('curl -F "data=@%s" -F "userid=%d" http://example.com', '@{"json":"data"}', 3); will automatically save first argument to file and insert its path

```php
function cmd(...$args)
```


### bg

Runs a process in the background

```php
function bg(...$args)
```


### cmd_capture

Captures the output of command using backticks and trims the output

```php
function cmd_capture(...$args)
```


### cmd_parallel

Runs commands in parallel using xargs


```php
function cmd_parallel(array $commands, int $max_parallel = 3)
```


Example:

```php
$o = cmd_parallel(['youtube-dl 1', 'youtube-dl 2', 'php sleep.php'], 3);
```

### cache_memory

Caches (read/write) value to memory


```php
function cache_memory($key, $value = NULL)
```


Example:

```php
$r = cache_memory('key', [1, 2]);
```

### cache_local

Caches (read/write) value to file cache


```php
function cache_local($key, $value = NULL, $expires = 86400, $refresh = FALSE)
```


Example:

```php
$r = cache_local('key', [1, 2], -86400, true); //fresh result - cache for 86400
```

### confirm

Waits for confirmation from command line

```php
function confirm($msg = 'Are you sure?', $default = 'y', $accept = '/^y/i')
```


### dump

Dumps variables to screen or browser (with proper formatting)

```php
function dump(...$args)
```


### encrypt_str

Encrypts a string

```php
function encrypt_str($string, $password = '')
```


### decrypt_str

Decrypts a string encoded with encrypt

```php
function decrypt_str($string, $password = '')
```


### is_cli

Checks if PHP is running in CLI mode

```php
function is_cli()
```


### get_argv

Gets an argument by name (or alias) from command line


 @return mixed

```php
function get_argv($name = NULL, $default = NULL)
```


Example:

```php
$val = get_argv('name', null);
```

### show_errors

Shows or hides all errors

```php
function show_errors($show = TRUE)
```


