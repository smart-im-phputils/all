<?php

# Array related

if (!function_exists('where')) {
    /**
     * return data matching specific key value condition
     *
     * $match = where([['a'=>1],['b'=>2]], ['b'=>2]);
     *
     * Array related
     */
    function where(array $array = [], array $cond = []) {
        $result = [];
        foreach ($array as $arrItem) {
            foreach ($cond as $condK => $condV) {
                if (!isset($arrItem[$condK]) || $arrItem[$condK] !== $condV) {
                    continue 2;
                }
            }
            $result[] = $arrItem;
        }
        return $result;
    }
}

if (!function_exists('randomize')) {
    /**
     * Returns the random version of array (original array is unchanged)
     */
    function randomize(array $array) {
        for ($i = 0, $c = \count($array); $i < $c - 1; $i++) {
            $j = \rand($i + 1, $c - 1);
            list($array[$i], $array[$j]) = [$array[$j], $array[$i]];
        }

        return $array;
    }
}

if (!function_exists('pluck')) {
    /**
     * Returns same key from all items of a multi-dimensional array
     */
    function pluck(array $array, string $match) {
        foreach ($array as $item) {
            foreach ($item as $key => $value) {
                if (strcasecmp($key, $match) === 0) {
                    $results[] = $value;
                }
            }
        }

        return $results ?? [];
    }
}

if (!function_exists('not_empty')) {
    /**
     * Opposite of empty. Returns $value if not empty, otherwise false.
     * Great for `if` loops. Also returns false if a hash has all empty values.
     *
     *$v = not_empty([]); // returns false
     *$v = not_empty(['a'=>'','b'=>'']); // returns false
     *$v = not_empty([1,2]); // returns [1,2]
     */
    function not_empty($value) {
        return empty($value) ? FALSE : (is_array($value) && empty(array_filter($value)) ? FALSE : $value);
    }
}

if (!function_exists('array_map_key_values')) {
    /**
     * Like array map but it's possible to change both keys and values (original array is unchanged)
     *
     * $v = array_map_key_values(function (&$k, &$v) { $k = "$k-a"; $v = "$v-3"; }, ['a' => 1, 'b' => 2, 'c' => 3]);
     */
    function array_map_key_values($closure, $arr) {
        foreach ($arr as $key => $value) {
            unset($arr[$key]);
            $closure($key, $value);
            $arr[$key] = $value;
        }

        return $arr;
    }
}

if (!function_exists('array_map_args')) {
    /**
     * Calls array_map function with args
     *
     * $trim = array_map_args('ltrim', ['/test', '/best'], '/'); //passes '/' to ltrim
     */
    function array_map_args($fn, $array, ...$args) {
        return array_map(function ($item) use ($fn, $args) {
            return call_user_func_array($fn, array_merge((array) $item, (array) $args));
        }, $array);
    }
}

if (!function_exists('sort_by_key')) {
    /**
     * Sorts an array by any key
     *
     * $v = sort_by_key([['name' => 'moe',   'age' => 40],['name' => 'larry', 'age' => 50],['name' => 'curly', 'age' => 60]], 'age', true);
     */
    function sort_by_key($arr, $key, $descending = FALSE) {
        usort($arr, function ($a, $b) use ($key, $descending) {
            return $descending ? $b[$key] <=> $a[$key] : $a[$key] <=> $b[$key];
        });

        return $arr;
    }
}

if (!function_exists('group_by')) {
    /**
     * Groups an array's item by any key
     *
     * $v = group_by([['name' => 'maciej',    'continent' => 'Europe'],['name' => 'yoan',      'continent' => 'Europe'],['name' => 'brandtley', 'continent' => 'North America']], 'continent');
     */
    function group_by($arr, $match) {
        foreach ($arr as $item) {
            foreach ($item as $key => $value) {
                if (strcasecmp($key, $match) === 0) {
                    $results[$value][] = $item;
                }
            }
        }

        return $results ?? [];
    }
}

if (!function_exists('chunk')) {
    /**
     * Breaks array into chunks
     *
     * $v = chunk([1, 2, 3, 4, 5], 3);
     * // → [[1, 2, 3], [4, 5]]
     */
    function chunk(array $array, $size = 1, $preserveKeys = FALSE) {
        return \array_chunk($array, $size, $preserveKeys);
    }
}

if (!function_exists('array_set')) {
    /**
     * Set an array item using dot.path notation
     *
     * $arr = []; array_set($arr, 'a.b.c.d', 11);
     */
    function array_set(array &$arr, $path, $val) {
        $loc = &$arr;

        foreach (explode('.', $path) as $step) {
            if (!empty($loc[$step]) && (!is_array($loc[$step]))) {
                $loc[$step] = [];
            }

            $loc = &$loc[$step];
        }

        return $loc = $val;
    }
}

if (!function_exists('array_get')) {
    /**
     * Gets an array item using dot.path notation
     *
     * $v = array_get($arr, 'a.b.c.d', 'default');
     */

    function array_get(array $arr, $path, $default = NULL) {
        if (empty($arr)) return $default;

        $p = &$arr;
        foreach (explode('.', $path) as $step) {
            if (isset($p[$step])) {
                $p = &$p[$step]; // if exists go to next level
            } else {
                return $default;
            }         // no such key
        }

        return $p;
    }
}

if (!function_exists('json_decode_array')) {
    /**
     * Converts string, object or stdClass into an array
     * ($preserve = true, leave empty objects "{}" as stdClass otherwise they convert to "[]")
     */
    function json_decode_array($obj, $preserve = TRUE) {
        $str = is_object($obj) ? json_encode($obj) : $obj;

        if (!$preserve) {
            return @json_decode($str, TRUE);
        } else {
            if ($arr = @json_decode($str)) {
                $toArr = function ($arr) use (&$toArr) {
                    foreach ($arr as $key => $value) {
                        $result[$key] = (is_scalar($value) || (empty((array) $value))) ? $value : (array) $toArr($value); //preserve empty objects as stdClass
                    }

                    return $result ?? [];
                };

                return is_scalar($arr) ? $arr : $toArr($arr);
            }
        }

        return FALSE;
    }
}

if (!function_exists('has_keys')) {
    /**
     * Check if the all keys exist in the array (and are not empty)
     *
     * Return value is hash with only keys
     */
    function has_keys($hash, ...$keys) {
        foreach ($keys as $key) {
            if (empty($hash[$key])) {
                return FALSE;
            } else {
                $results[$key] = $hash[$key];
            }
        }

        return $results ?? [];
    }
}

if (!function_exists('pluck_keys')) {
    /**
     * Returns only selected keys from a hash (including empty keys)
     */
    function pluck_keys($hash, ...$keys) {
        return array_intersect_key($hash, array_flip(array_flatten($keys))) ?: [];
    }
}

if (!function_exists('array_flatten')) {
    /**
     * Flattens a multi-dimensional array
     */
    function array_flatten(array $array) {
        foreach (new RecursiveIteratorIterator(new RecursiveArrayIterator($array)) as $v) {
            $results[] = $v;
        }

        return $results ?? [];
    }
}

if (!function_exists('pluck_keys_ignore_empty')) {
    /**
     * Returns only selected keys from a hash (ignores empty values)
     */
    function pluck_keys_ignore_empty($hash, ...$keys) {
        return array_filter(call_user_func_array('pluck_keys', array_merge([$hash], $keys)));
    }
}

if (!function_exists('is_hash')) {
    /**
     * Check if an array is associative
     */
    function is_hash(array $arr) {
        if ([] === $arr) return FALSE;
        return array_values($arr) !== $arr;
    }
}

if (!function_exists('is_email')) {
    /**
     * Checks if the string is an email (returns $email if true otherwise false)
     */
    function is_email($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}

if (!function_exists('is_phone')) {
    /**
     * Checks if the string is a phone number (returns $phone if true otherwise false)
     */
    function is_phone($phone) {
        return preg_match('/^[\+0-9 ]{9,}$/', $phone) ? $phone : FALSE;
    }
}

if (!function_exists('is_url')) {
    /**
     * Checks if the string is a url (returns $url if true otherwise false)
     */
    function is_url($url) {
        return filter_var($url, FILTER_VALIDATE_URL);
    }
}

if (!function_exists('array_first')) {
    /**
     * Gets the first item of array or hash
     *
     * $first = array_first([1,2,3]) => 1;
     *
     * @return array|string
     */
    function array_first($array, $reverse = FALSE) {
        if (!empty($array)) {
            if (is_hash($array)) {
                $keys = array_keys($array);
                $index = $reverse ? count($keys) - 1 : 0;
                return [$keys[$index], $array[$keys[$index]]];
            } else {
                $index = $reverse ? count($array) - 1 : 0;
                return $array[$index];
            }
        }

        return NULL;
    }
}

if (!function_exists('array_last')) {
    /**
     * Get the last item of array or hash
     *
     * $last = array_first([1,2,3], true) => 3;
     */
    function array_last($array) {
        return array_first($array, TRUE);
    }
}

if (!function_exists('array_filter_regex')) {
    /**
     * Filters all array items by regex
     *
     * $php = array_filter_regex('/\.php$/', $files);
     */
    function array_filter_regex($regex, $array) {
        return array_values(array_filter(array_map(function ($i) use ($regex) { return preg_match($regex, $i) ? $i : FALSE; }, $array)));
    }
}

if (!function_exists('array_except')) {
    /**
     * Removes selected items from array / or keys from hash
     */
    function array_except(array $arr, ...$args) {
        $is_hash = is_hash($arr);
        return array_filter($arr, function ($v, $k) use ($is_hash, $args) {
            return !in_array($is_hash ? $k : $v, $args);
        }, ARRAY_FILTER_USE_BOTH);
    }
}

if (!function_exists('in_array_nc')) {
    /**
     * Case in-sensitive in_array function
     */
    function in_array_nc($needle, $haystack) {
        return in_array(strtolower($needle), array_map('strtolower', (array) $haystack));
    }
}

# String related

if (!function_exists('lines')) {
    /**
     * Breaks string into array
     *
     * $lines = lines("this\nis\n\na\r\ntest");
     *
     * String related
     */
    function lines($str, $delim = '\r?\n') {
        return array_filter(array_map('trim', preg_split("/$delim/", $str)));
    }
}

if (!function_exists('single_line')) {
    /**
     * Convert multi-line text into a single line
     *
     * $merge = single_line("this\nis\n\na\ntest", " - ");
     */
    function single_line($text, $delim = '') {
        return join($delim, lines(is_scalar($text) ? $text : json_encode($text)));
    }
}
if (!function_exists('ansi')) {
    /**
     * Remove non-ansi characters from string
     */
    function ansi($str) {
        return iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $str);
    }
}

if (!function_exists('slugify')) {
    /**
     * Creates a slugified version of a word or phrase
     *
     * $unique = slugify('this is a test', true = adds a number or increments existing number);
     */
    function slugify($text, $unique = FALSE) {
        $text = ansi($text);
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, '-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);

        if ($unique) {
            $num = preg_match('/\-(\d+)$/', $text, $m) ? $m[1] : 0;
            $text = sprintf('%s-%0' . strlen($num) . 'd', preg_replace('/\-(\d+)$/', '', $text), $num + 1);
        }

        return empty($text) ? 'n-a' : $text;
    }
}

if (!function_exists('password')) {
    /**
     * Generates a random password
     */
    function password($length = 16) {
        $string = '';

        while (($len = strlen($string)) < $length) {
            $size = $length - $len;

            try {
                $bytes = random_bytes($size);
            } catch (Exception $e) {
            }

            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }

        return $string;
    }
}

if (!function_exists('str_insert')) {
    /**
     * Inserts a string after a matching phrase in an existing string
     */
    function str_insert($document, $match, $insert, $ignore_case = FALSE, $replace = FALSE) {
        if (!($index = $ignore_case ? stripos($document, $match) : strpos($document, $match))) {
            return $document;
        }

        return substr_replace($document, $replace ? $insert : $match . $insert, $index, strlen($match));
    }
}

if (!function_exists('str_replace_block')) {
    /**
     * Replaces a block between matching tags with an existing string
     *
     * $out = str_replace_block('data', '// ** start', '// ** end', 'new content', false, false);
     */
    function str_replace_block($document, $match_start, $match_end, $insert, $ignore_case = FALSE, $replace = FALSE) {
        $index_start = $ignore_case ? stripos($document, $match_start) : strpos($document, $match_start);
        $index_end = $ignore_case ? stripos($document, $match_end) : strpos($document, $match_end);

        if (!$index_start || !$index_end) {
            return $document;
        }

        return substr_replace($document, $replace ? $insert : $match_start . $insert . $match_end, $index_start, $index_end + strlen($match_end) - $index_start);
    }
}

if (!function_exists('str_wrap')) {
    /**
     * Wraps a string inside characters (return '' if string is blank) - automatically determins the right char is NULL
     *
     * $str = str_wrap('test', "<", ">", true); => <test>, $str = str_wrap('ok', '"'); => "ok", $str = str_wrap('', '<b>'); => ''
     */
    function str_wrap($input, $left, $right = NULL, $wrap_blank = FALSE) {
        if (empty($input) && empty($wrap_blank)) {
            return '';
        }

        if ($right === NULL) {
            $right = preg_match('/^\<(.*?)\>$/', $left, $matches) ? "</$matches[1]>" : $left;
        }

        $results = [];

        foreach ((array) $input as $str) {
            $str = preg_replace("/^" . preg_quote($left, '/') . "/", '', $str, 1);
            $str = preg_replace("/" . preg_quote($right, '/') . "$/", '', $str, 1);

            $results[] = sprintf('%s%s%s', $left, trim($str), $right);
        }

        return is_array($input) ? $results : ($results[0] ?? '');
    }
}

if (!function_exists('str_quote')) {
    /**
     * Put quotes around a string (or items of an array)
     *
     * $arr = str_quote([1, 'one ', '"two"']); //["1", "one", "two"]
     */
    function str_quote($input, $char = '"', $trim = TRUE) {
        $results = [];

        foreach ((array) $input as $str) {
            $str = preg_replace("/^$char/", '', $str, 1);
            $str = preg_replace("/$char$/", '', $str, 1);

            $results[] = sprintf('%s%s%s', $char, addslashes($trim ? trim($str) : $str), $char);
        }

        return is_array($input) ? $results : ($results[0] ?? '');
    }
}

if (!function_exists('str_match_all_words')) {
    /**
     * Returns true if a string contains all the words
     *
     * $match = str_match_all_words('this is a test', ['/Th.s/i', 'a', 'test'], true, true, true); //order = words must be in order. Note: 1st word is regex, 2nd is a whole word.
     */
    function str_match_all_words($str, array $words, $whole_words = TRUE, $ignore_case = TRUE, $order = FALSE) {
        foreach ($words as $word) {
            $regex = preg_match('#^/(.*)(/(\w+)?)$#', $word, $matches) ? $word : sprintf("/(%s)/%s", sprintf($whole_words ? "\b%s\b" : "%s", preg_quote($word, '/')), $ignore_case ? 'i' : '');

            #echo $regex, $offset ?? 0, "\n";
            if (preg_match($regex, $str, $matches, PREG_OFFSET_CAPTURE, $order ? ($offset ?? 0) : 0)) {
                $offset = $matches[0][1] + strlen($matches[0][0]);
            } else {
                return FALSE;
            }
        }

        return TRUE;
    }
}

if (!function_exists('str_match_brackets')) {
    /**
     * Returns all the balanced matching brackets in the string
     */
    function str_match_brackets($str, $bracket = '{') {
        $delimiter_left = $bracket;
        $delimiter_right = $bracket === '{' ? '}' : ($bracket === '(' ? ')' : ($bracket === '[' ? ']' : $bracket));
        $delimiter_wrap = '~';

        $delimiter_left = preg_quote($delimiter_left, $delimiter_wrap);
        $delimiter_right = preg_quote($delimiter_right, $delimiter_wrap);
        $pattern = $delimiter_wrap . $delimiter_left
            . '((?:[^' . $delimiter_left . $delimiter_right . ']++|(?R))*)'
            . $delimiter_right . $delimiter_wrap;

        return preg_match_all($pattern, $str, $matches) ? $matches[0] : FALSE;
    }
}

if (!function_exists('next_version')) {
    /**
     * Increase the version number by 1
     */
    function next_version($version) {
        $version = preg_replace('/[^0-9\.]/', '', $version ?: '0.0.1');
        $parts = explode('.', $version);

        if ($parts[2] + 1 < 99) {
            $parts[2]++;
        } else {
            $parts[2] = 0;
            if ($parts[1] + 1 < 99) {
                $parts[1]++;
            } else {
                $parts[1] = 0;
                $parts[0]++;
            }
        }

        return implode('.', $parts);
    }
}

if (!function_exists('kebab')) {
    /**
     * Coverts any word into kebab-case
     */
    function kebab($input, $delim = '-') {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];

        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }

        return implode($delim, $ret);
    }
}

if (!function_exists('camel')) {
    /**
     * Coverts any word to camelCase
     */
    function camel($string) {
        $str = str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $string)));
        $str[0] = strtolower($str[0]);

        return $str;
    }
}

if (!function_exists('unslugify')) {
    /**
     *  Converts text in camel/kebab/underscore case to normal text
     */
    function unslugify($slug, $ucwords = FALSE) {
        $text = trim(preg_replace('/\s+/', ' ', preg_replace('/[^A-Za-z0-9\']+/', ' ', preg_replace('/(?!^)[A-Z]{2,}(?=[A-Z][a-z])|[A-Z][a-z]/', ' $0', $slug))));
        return $ucwords ? ucwords($text) : strtolower($text);
    }
}

if (!function_exists('remove_parens')) {
    /**
     * Returns any text inside parenthesis from a string
     *
     * $str = remove_parens("Hustlin' ((Remix) Album Version (Explicit)) with (a(bbb(ccc)b)a) speed!");
     *
     */
    function remove_parens($string) {
        return preg_replace("/\(([^()]|(?R))*\)/", "", $string);
    }
}

if (!function_exists('template')) {
    /**
     * Replaces any tags inside template
     *
     * $out = template('hi {{first}}, today is {{date}}', ['first' => 'san', 'date' => today()]);
     */
    function template($str, $tags = [], $ignore_missing = FALSE) {
        return preg_replace_callback('~{{([^}]*)}}~', function ($m) use ($tags, $ignore_missing) {
            return isset($tags[$m[1]]) ? $tags[$m[1]] : ($ignore_missing ? $m[0] : '');
        }, $str);
    }
}

if (!function_exists('file_template')) {
    /**
     * Same as template, except it uses a file instead of string
     */
    function file_template($path, $tags = []) {
        return template(file_get_contents($path), $tags);
    }
}

if (!function_exists('bool')) {
    /**
     * Converts a string value to it's equivanelt boolean
     */
    function bool($str) {
        return filter_var($str, FILTER_VALIDATE_BOOLEAN);
    }
}

if (!function_exists('eval_php')) {
    /**
     * Evaluates a PHP string like include_once
     *
     * $result = eval_php('Hi <?php echo 1; ?>!', get_defined_vars()); //Hi 1!
     */
    function eval_php($code, $vars = []) {
        try {
            ob_start();

            if (!empty($vars)) {
                extract($vars);
            }

            eval('?>' . $code . '<?php ');
            $result = ob_get_contents();
        } catch (\Exception $e) {
            debug('Error in eval: ' . $e->getMessage());
        } finally {
            ob_end_clean();
        }

        return $result ?? '';
    }
}

if (!function_exists('words')) {
    /**
     * Splits a sentence in tor words
     *
     * $v = words('this is mr.san "hello" there!');
     */
    function words($text) {
        return preg_split('/((^\p{P}+)|(\p{P}*\s+\p{P}*)|(\p{P}+$))/', $text, -1, PREG_SPLIT_NO_EMPTY);
    }
}

if (!function_exists('sentences')) {
    /**
     * Splits a paragraph into sentences
     *
     * $v = sentences('this is a test. hello dr.dre, how are you? ok, bye.');
     */
    function sentences($str) {
        return preg_split('/(?<=[.?!])\s+(?=[a-z])/i', $str);
    }
}

if (!function_exists('split_name')) {
    /**
     * Splits a name into first and last
     */
    function split_name($name, $default = 'Member') {
        if (@preg_match('#^(\w+\.)?\s*([\'\’\w]+)\s+([\'\’\w]+)\s*(\w+\.?)?$#', $name, $results)) {
            if (!empty($results[2])) {
                return ['first' => $results[2], 'last' => $results[3]];
            }
        }

        return ['first' => $default, 'last' => ''];
    }
}

if (!function_exists('stop_words')) {
    /**
     * Lists most common English stopwords
     */
    function stop_words() {
        return cache_local('stop_words', function () { return lines(curl('https://raw.githubusercontent.com/Alir3z4/stop-words/master/english.txt')); }, 86400 * 365);
    }
}

if (!function_exists('extract_email')) {
    /**
     * Extracts an email address from text
     */
    function extract_email($text, $single = TRUE) {
        if (preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $text, $matches)) {
            return $single ? $matches[0][0] : $matches[0];
        }

        return NULL;
    }
}

if (!function_exists('email_format')) {
    /**
     * Convert name, email to "name" <email>
     */
    function email_format($email, $name = '') {
        return sprintf('"%s" <%s>', $name ?: 'Member', extract_email($email));
    }
}

if (!function_exists('html2text')) {
    /**
     * Converts html to text
     */
    function html2text($html, $extract_links = TRUE) {
        $str = "<body>" . strip_tags(preg_replace('#<br/?>#', "\n", $html), '<b><a><i><div><span><p>') . "</body>";
        $xpath = dom_path($str);

        if (!empty($extract_links)) {
            $links = $xpath->query('//a');
            foreach ($links as $link) {
                $link->textContent = $link->textContent . " (" . $link->getAttribute("href") . ")\n\n";
            }
        }

        $node = $xpath->query('body')->item(0);
        return preg_replace("/\r?\n{2,}/", "\n\n", $node->textContent); // text
    }
}

if (!function_exists('cookie_get_json')) {
    /**
     * Gets the cookie value or returns $default using JSON format
     */
    function cookie_get_json($name, $default = []) {
        return json_decode($_COOKIE[$name] ?? '', TRUE) ?: $default;
    }
}

if (!function_exists('cookie_set_json')) {
    /**
     * Sets a cookie in browser using JSON format
     */
    function cookie_set_json($name, $value, $expires = '+1 day', $append = FALSE, $domain = '') {
        if ($append) {
            $prev = cookie_get_json($name);

            if (is_array($prev)) {
                $value = array_merge($prev, (array) $value);
            } else {
                debug('Cannot append to non-array values');
            }
        }

        return setcookie($name, json_encode($value), strtotime($expires), '/', '.' . domain($domain ?: $_SERVER['HTTP_HOST']));
    }
}

if (!function_exists('self_uri')) {
    /**
     * Return the (sanitized) URL of current page
     */
    function self_uri($params = FALSE) {
        $host = rtrim($_SERVER['HTTP_HOST'] ?? 'localhost', '/') . '/';
        $uri = ltrim(empty($params) ? parse_url($_SERVER['REQUEST_URI'] ?? '', PHP_URL_PATH) : $_SERVER['REQUEST_URI'] ?? '', '/');
        return @filter_var((isset($_SERVER['HTTPS']) ? "https" : "http") . "://$host$uri", FILTER_SANITIZE_URL);
    }
}

if (!function_exists('absolute_uri')) {
    /**
     * Appends the http part if not already present in uri
     */
    function absolute_uri($path, $host = '') {
        $host = $host ?: self_uri();
        $info = parse_url("http://$path");

        return is_url($path) ? $path : (preg_match('/^(?:[-A-Za-z0-9]+\.)+[A-Za-z]{2,6}$/', $info['host']) ? sprintf("%s://%s", (isset($_SERVER['HTTPS']) ? "https" : "http"), $path) : sprintf('%s/%s', rtrim($host, '/'), ltrim($path, '/')));
    }
}

if (!function_exists('url_append_params')) {
    /**
     * Appends new params at the end of existing URL
     */
    function url_append_params($url, $params = []) {
        $p = parse_url($url);
        $q = !empty($p['query']);

        return @sprintf('%s://%s/%s%s%s%s%s%s%s', $p['scheme'], $p['host'], ltrim($p['path'], '/'), $q ? '?' : '', $p['query'], $q ? '&' : '?', http_build_query($params), !empty($p['fragment']) ? '#' : '', $p['fragment']);
    }
}

# File related

if (!function_exists('file_name')) {
    /**
     * Returns the name of file without extension
     *
     * $name = file_name('c:/test.jpg'); //test
     */
    function file_name($path) {
        return pathinfo($path, PATHINFO_FILENAME);
    }
}

if (!function_exists('file_ext')) {
    /**
     * Returns the extension of file (without the dot)
     *
     * $ext = file_name('c:/test.jpg'); //jpg
     */
    function file_ext($path) {
        return pathinfo($path, PATHINFO_EXTENSION);
    }
}

if (!function_exists('change_ext')) {
    /**
     * Changes file extension (and optionally path)
     *
     * $path = change_ext('c:/d/tmp.txt', 'mp3', [new_path]); # c:/d/tmp.mp3
     */
    function change_ext($path, $new_ext, $new_path = '') {
        return sprintf('%s/%s.%s', !empty($new_path) ? rtrim($new_path, '/') : pathinfo($path, PATHINFO_DIRNAME), pathinfo($path, PATHINFO_FILENAME), ltrim($new_ext, '.'));
    }
}

if (!function_exists('file_arr')) {
    /**
     * Reads a file into an array
     *
     * $arr = file_arr('some.txt');
     */
    function file_arr($path) {
        return lines(file_get_contents($path));
    }
}

if (!function_exists('read_csv')) {
    /**
     * Reads a csv file (with header support)
     *
     * $arr = read_csv('file', ['col1', 'col2'] | true = first line is header | false = no header);
     *
     * File related
     */
    function read_csv($path, $header = FALSE, $delim = ',') {
        $csv = @array_map(function ($str) use ($delim) { return array_map('trim', str_getcsv($str, $delim)); }, file($path, FILE_SKIP_EMPTY_LINES));

        if (!empty($header)) {
            $keys = is_array($header) ? $header : array_shift($csv);
            foreach ($csv as $i => $row) {
                $csv[$i] = array_combine($keys, $row);
            }
        }

        return $csv;
    }
}

if (!function_exists('read_json')) {
    /**
     * Reads a json file (optionally returning a property using dot path notation)
     *
     * $prop = read_json('file'[, 'package.details.name', 'untitled'\);
     */
    function read_json($path, $prop = NULL, $default = NULL) {
        if (file_exists($path)) {
            if ($contents = file_get_contents($path)) {
                $data = json_decode_array($contents, TRUE);
                return !empty($prop) ? array_get($data, $prop, $default) : $data;
            }
        }

        return $default;
    }
}

if (!function_exists('fix_json')) {
    /**
     * Converts a Javascript object into valid JSON
     */
    function fix_json($string, $fixNames = TRUE, $asArray = TRUE) {
        if (strpos($string, '(') === 0) {
            $string = substr($string, 1, strlen($string) - 2); // remove outer ( and )
        }

        if ($fixNames) {
            $string = preg_replace("/(?<!\"|'|\w)([a-zA-Z0-9_]+?)(?!\"|'|\w)\s?:/", "\"$1\":", $string);
        }

        if ($fixNames) {
            $string = preg_replace("/(?<!\"|'|\w)(String|Boolean|Number|Object|Array)(?!\"|'|\w)\s?/", "\"$1\"", $string);
        }

        //fix trailing comma
        $string = preg_replace('/(\"|\]|\}|String|Boolean|Number|Object|Array|false)\s*,(\s*(?=[}\]]))/ms', '$1$2', $string);

        $regex = <<<'REGEX'
~
    "[^"\\]*(?:\\.|[^"\\]*)*"
    (*SKIP)(*F)
  | '([^'\\]*(?:\\.|[^'\\]*)*)'
~x
REGEX;

        $string = preg_replace_callback($regex, function ($matches) {
            return '"' . preg_replace('~\\\\.(*SKIP)(*F)|"~', '\\"', $matches[1]) . '"';
        }, $string);

        return json_decode($string, $asArray);
    }
}

if (!function_exists('read_config')) {
    /**
     * Parses env file into a hash
     */
    function read_config($path, $name = NULL) {
        $data = parse_ini_file($path, FALSE, INI_SCANNER_RAW);
        $hash = !empty($data) ? $data : [];

        return !empty($name) ? ($hash[$name] ?? '') : $hash;
    }
}

if (!function_exists('dot_env')) {
    /**
     * Reads a .env file into enviroment variables
     */
    function dot_env($path) {
        foreach (read_config($path) as $key => $value) {
            putenv(sprintf('%s=%s', $key, $value));
            $_ENV[$key] = $value;
        }
    }
}

if (!function_exists('write_file')) {
    /**
     * Writes to
     * a file (optionally json_encoding any non-scalar data) and returns filename on success
     */
    function write_file($fn, $data, $append = FALSE) {
        @mkdir(dirname($fn), 0777, TRUE);

        if (file_put_contents($fn, is_scalar($data) ? $data : json_encode($data), $append ? FILE_APPEND : 0)) {
            return $fn;
        }

        return FALSE;
    }
}

if (!function_exists('write_json')) {
    /**
     * Updates an existing json file setting (array of) properties using dot path notation
     *
     * $result = write_json('file', ['package.details.name' => 'test', 'version' => 2][, 'save-as']); //true or false
     */
    function write_json($path, array $props, $saveAs = NULL) {
        if ($data = read_json($path)) {
            foreach ($props as $key => $value) {
                array_set($data, $key, $value);
            }

            $out = empty($saveAs) ? $path : (dirname($saveAs) === '.' ? sprintf('%s/%s', dirname($path), $saveAs) : $saveAs);

            return file_put_contents($out, json_encode($data, JSON_PRETTY_PRINT)) ? $data : FALSE;
        }

        return FALSE;
    }
}

if (!function_exists('jwt_encode')) {
    /**
     * JWT encodes a payload with $key
     *
     * $token = jwt_encode(['user_id' => 1][, 'secret' | env(APP_KEY)]);
     */
    function jwt_encode($payload, $key = '') {
        $key = $key ?: getenv('APP_KEY') ?: die('no key given');
        $headers = ['alg' => 'HS256', 'typ' => 'JWT'];
        $encode = function ($data) { return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); };
        $headers_encoded = $encode(json_encode($headers));
        $payload_encoded = $encode(json_encode($payload));

        $signature = hash_hmac('SHA256', "$headers_encoded.$payload_encoded", $key, TRUE);
        $signature_encoded = $encode($signature);

        return "$headers_encoded.$payload_encoded.$signature_encoded";
    }
}

if (!function_exists('jwt_decode')) {
    /**
     * JWT decodes a payload verifies it with $key
     *
     * $result = jwt_decode('token'[, 'secret' | env(APP_KEY)]);
     */
    function jwt_decode($jwt, $key = '') {
        if (!empty($jwt)) {
            list(, $bodyb64) = explode('.', $jwt, 3);

            if ($payload = not_empty(json_decode_array(base64_decode($bodyb64), TRUE))) {
                if (jwt_encode($payload, $key) === $jwt) {
                    if (empty($payload['exp']) || ($payload['exp'] >= time())) {
                        return $payload;
                    }
                }
            }
        }

        return FALSE;
    }
}

if (!function_exists('unix_path')) {
    /**
     * Converts everything to forward slashes
     */
    function unix_path($path) {
        return strtr($path, ['\\' => '/']);
    }
}

if (!function_exists('dos_path')) {
    /**
     * Converts everything to back slashes
     */
    function dos_path($path) {
        return strtr($path, ['/' => '\\']);
    }
}

if (!function_exists('relative_path')) {
    /**
     * Converts absolute path into relative path
     *
     * $relPath = relative_path('d:/songs', 'd:/songs/coldplay/amsterdam.txt');
     */
    function relative_path($docRoot, $path) {
        $docRoot = strtolower(unix_path(rtrim($docRoot, '/')) . '/');
        $path = unix_path($path);
        $path = strtolower(substr($path, 0, strlen($docRoot))) . substr($path, strlen($docRoot));

        $sourceDirs = explode('/', isset($docRoot[0]) && '/' === $docRoot[0] ? substr($docRoot, 1) : $docRoot);
        $targetDirs = explode('/', isset($path[0]) && '/' === $path[0] ? substr($path, 1) : $path);
        array_pop($sourceDirs);
        $targetFile = array_pop($targetDirs);

        foreach ($sourceDirs as $i => $dir) {
            if (isset($targetDirs[$i]) && $dir === $targetDirs[$i]) {
                unset($sourceDirs[$i], $targetDirs[$i]);
            } else {
                break;
            }
        }

        $targetDirs[] = $targetFile;
        $path = str_repeat('../', count($sourceDirs)) . implode('/', $targetDirs);

        return '' === $path || '/' === $path[0] || FALSE !== ($colonPos = strpos($path, ':')) && ($colonPos < ($slashPos = strpos($path, '/')) || FALSE === $slashPos) ? "./$path" : $path;
    }
}

if (!function_exists('temp_file')) {
    /**
     * Creates a temporary file with given extension and prefix
     */
    function temp_file($ext = 'tmp', $prefix = 'tmp') {
        return preg_replace('/\.tmp$/', '.' . ltrim($ext, '.'), tempnam(sys_get_temp_dir(), $prefix));
    }
}

if (!function_exists('image_size')) {
    /**
     * Get [width, height] of images
     */
    function image_size($path) {
        list($width, $height) = getimagesize($path);
        return [$width, $height];
    }
}

if (!function_exists('placeholder_image')) {
    /**
     * Returns a random placeholder image
     */
    function placeholder_image($width = 320, $height = 200, $type = 'any') {
        return sprintf('https://placeimg.com/%d/%d/%s?r=%s', $width, $height, $type, password(4));
    }
}

if (!function_exists('make_thumb')) {
    /**
     * Creates a thumbnail from any image
     */
    function make_thumb($src, $dest, $max_width = 100, $max_height = 100) {
        $source_image = imagecreatefromstring(file_get_contents($src));
        $width = imagesx($source_image);
        $height = imagesy($source_image);

        $ratio = min(1, $max_height / $height, $max_width / $width);

        $desired_width = floor($width * $ratio);
        $desired_height = floor($height * $ratio);
        $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

        return preg_match('/^(jpg|jpeg)$/i', pathinfo($dest, PATHINFO_EXTENSION)) ? imagejpeg($virtual_image, $dest) : imagepng($virtual_image, $dest);
    }
}

if (!function_exists('home_dir')) {
    /**
     * Get current users's home directory
     */
    function home_dir() {
        $home = getenv('HOME');

        if (!empty($home)) {
            $home = rtrim($home, '/');
        } elseif (!empty($_SERVER['HOMEDRIVE']) && !empty($_SERVER['HOMEPATH'])) {
            $home = $_SERVER['HOMEDRIVE'] . $_SERVER['HOMEPATH'];
            $home = rtrim($home, '\\/');
        }

        return empty($home) ? NULL : $home;
    }
}

if (!function_exists('directory')) {
    /**
     * Returns all files in the directory
     *
     * $files = directory('c:/tmp', depth [0 = no recursion, 1 = 1 level, -1 infinite], '/\.php$/' (optional regex for basename), [..dirs to exclude]);
     */
    function directory($path, $depth = -1, $regex = NULL, $exclude = [], $sort = FALSE, $includeDirs = FALSE) {
        /**
         * @param SplFileInfo                     $file
         * @param mixed                           $key
         * @param RecursiveCallbackFilterIterator $iterator
         *
         * @return bool True if you need to recurse or if the item is acceptable
         */
        $filter = function ($file, $key, $iterator) use ($exclude) {
            if ($iterator->hasChildren() && !in_array($file->getFilename(), $exclude)) {
                return TRUE;
            }

            return $file->isFile();
        };

        $rii = new RecursiveIteratorIterator(new RecursiveCallbackFilterIterator(new RecursiveDirectoryIterator($path), $filter), RecursiveIteratorIterator::CHILD_FIRST);
        $rii->setMaxDepth($depth);

        // @var \SplFileInfo $file
        foreach ($rii as $file) {
            if ($file->isDir() && !$includeDirs) {
                continue;
            }

            if (empty($regex) || preg_match("$regex", $file->getBasename())) {
                $files[] = $file->getRealPath();
            }
        }

        if (!empty($sort) && !empty($files)) {
            if ($sort === 'name') {
                usort($files, function ($a, $b) { return basename($a) <=> basename($b); });
            } else {
                usort($files, function ($a, $b) { return filemtime($a) < filemtime($b); });
            }
        }

        return $files ?? [];
    }
}

if (!function_exists('deltree')) {
    /**
     * Delete a directory RECURSIVELY
     */
    function deltree($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        rrmdir($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }

            reset($objects);
            rmdir($dir);
        }
    }
}

if (!function_exists('mime_type')) {
    /**
     * Returns mime type of any file
     *
     * $mime = mime_type('test.jpg');
     *
     */
    function mime_type($fileName) {
        return (preg_match('/\.(png|jpg|jpeg|gif|bmp)$/i', $fileName, $m) ? "image/$m[1]" : (preg_match('/\.(mp3|wav|aac|ogg)$/i', $fileName, $m) ? "audio/$m[1]" : (preg_match('/\.(avi|mov|mp4|wmv|mpeg)$/i', $fileName, $m) ? "video/$m[1]" : (preg_match('/\.(html|html)$/i', $fileName, $m) ? "text/html" : (preg_match('/\.(json|zip|7z|pdf|zip)$/i', $fileName, $m) ? "application/$m[1]" : (preg_match('/\.(txt|md)$/i', $fileName, $m) ? "text/plain" : (preg_match('/\.(css)$/i', $fileName, $m) ? "text/css" : (preg_match('/\.(js)$/i', $fileName, $m) ? "application/javascript" : (preg_match('/\.(woff|ttf|otf)$/i', $fileName, $m) ? "font/$m[1]" : 'application/octet-stream')))))))));
    }
}

# Internet related

if (!function_exists('basename_url')) {
    /**
     * Returns the last part of URL without query string
     */
    function basename_url($url) {
        return basename(parse_url($url, PHP_URL_PATH));
    }
}

if (!function_exists('curl')) {
    /**
     * Performs a GET/POST request with $params using CURL
     *
     * $response = url_get('http://www.google.com/search', 'get', ['q' => 'test'] | json (post body), ['header1', 'header2'], &$status);
     *
     * Internet related
     */
    function curl($url, $method = 'get', $params = [], $headers = [], &$status = NULL) {
        $ch = curl_init("");
        $result = parse_url($url);
        $host = $result['scheme'] . "://" . $result['host'];
        $timeout = max(5, getenv('CURL_TIMEOUT') ?: 30);

        if (strcasecmp($method, 'post') === 0) {
            curl_setopt($ch, CURLOPT_POST, 1);

            if (is_array($params)) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
            } elseif (is_scalar($params)) {
                @json_decode($params);
                if (json_last_error() === 0) {
                    $headers[] = 'Content-Type: application/json';
                }

                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            }
        } elseif (strcasecmp($method, 'get') === 0) {
            $url = sprintf("%s%s%s", $url, strpos($url, '?') === FALSE ? '?' : '&', http_build_query($params));
        } else {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
            curl_setopt($ch, CURLOPT_POSTFIELDS, is_array($params) ? http_build_query($params) : $params);
        }

        if (env('CURL_DEBUG')) {
            debug("curl: $url, ($timeout sec)");
        }

        curl_setopt($ch, CURLOPT_URL, $url);

        if (!empty($headers)) {
            if (is_hash($headers)) {
                debug('Headers should be an array not hash!');
            }

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($ch, CURLOPT_USERAGENT, "Opera/9.80 (J2ME/MIDP; Opera Mini/4.2.14912/870; U; id) Presto/2.4.15");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_REFERER, $host);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status !== 200) {
            debug($output);
        }

        curl_close($ch);

        return $output;
    }
}

if (!function_exists('upload')) {
    /**
     * Saves data to Amazon S3
     */
    function upload($data, $fileName, $bucket, $mime = '', $acl = 'public-read') {
        $date = date('r');
        $priv_key = getenv('AWS_SECRET_ACCESS_KEY') ?: die('aws secret key not present');
        $access_key = getenv('AWS_ACCESS_KEY_ID') ?: die('aws access id not present');
        $mime = $mime ?: mime_type($fileName);
        $acl = "x-amz-acl:$acl";
        $signature1 = base64_encode(hash_hmac('sha1', "PUT\n\n$mime\n$date\n$acl\n/$bucket/$fileName", $priv_key, TRUE));
        $endpoint = "https://$bucket.s3.amazonaws.com/$fileName";

        $headers = [
            "Host: $bucket.s3.amazonaws.com",
            "Date: $date",
            "Content-Type: $mime",
            "Content-Length: " . strlen($data),
            "Authorization: AWS $access_key:$signature1",
            $acl,
        ];

        curl($endpoint, 'put', $data, $headers, $status);
        return $status === 200 ? "https://s3.amazonaws.com/$bucket/$fileName" : FALSE;
    }
}

if (!function_exists('upload_file')) {
    /**
     * Uploads a file to Amazon S3
     */
    function upload_file($file, $bucket, $path = '', $mime = '', $acl = 'public-read') {
        return upload(file_get_contents($file), $path ?: basename($file), $bucket, $mime, $acl);
    }
}

if (!function_exists('s3_contents')) {
    /**
     * Lists the contents of a S3 bucket
     *
     * $list = s3_contents('bucket', 'dir'); // returns all files in dir, $file_data = s3_contents('bucket', '', 'path_to_file'); // returns file's data instead
     */
    function s3_contents($bucket, $path = '', $fileName = '/') {
        do {
            $date = date('r');
            $acl = "x-amz-acl:public-read";
            $content_type = 'application/json';
            $priv_key = getenv('AWS_SECRET_ACCESS_KEY') ?: die('aws secret key not present');
            $access_key = getenv('AWS_ACCESS_KEY_ID') ?: die('aws access id not present');
            $signature1 = base64_encode(hash_hmac('sha1', "GET\n\n$content_type\n$date\n$acl\n/$bucket$fileName", $priv_key, TRUE));
            $endpoint = "https://$bucket.s3.amazonaws.com$fileName?" . http_build_query(array_filter(['list-type' => 2, 'prefix' => $path, 'continuation-token' => $token ?? '']));
            //list-type=2" . (!empty($path) ? "&prefix=$path" : '') . (!empty($token) ? "&continuation-token=" . urlencode($token) : '');

            $headers = [
                "Host: $bucket.s3.amazonaws.com",
                "Date: $date",
                "Content-Type: $content_type",
                "Authorization: AWS $access_key:$signature1",
                $acl,
            ];

            $result = curl($endpoint, 'get', [], $headers, $status);
            $token = '';

            if ($status === 200) {
                if (!empty(ltrim($fileName, '/'))) {
                    return $result;
                } else {
                    try {
                        $xml = simplexml_load_string($result);
                        file_put_contents('c:/tmp/d.xml', $result);
                        $token = $xml->NextContinuationToken;
                        $tags = $xml->xpath('*');

                        /** @var \SimpleXMLElement $tag */
                        foreach ($tags as $tag) {
                            if ($tag->getName() === 'Contents') {
                                /** @var \SimpleXMLElement $key */
                                $results[] = "https://s3.amazonaws.com/$bucket/$tag->Key";
                            }
                        }
                    } catch (\Exception $e) {
                        $token = NULL;
                    }
                }
            }
        } while (!empty($token));

        return $results ?? [];
    }
}

if (!function_exists('decode_image_data')) {
    /**
     * Decodes base64 image data (generated via HTML5 Canvas)
     *
     * $data = decode_image_data('data:image/png;base64,AAAFBfj42Pj4'[, $type]);
     */
    function decode_image_data($data, &$type = []) {
        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif

            if (!in_array($type, ['jpg', 'jpeg', 'gif', 'png'])) {
                return FALSE;
            }

            return base64_decode($data);
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('gravatar')) {
    /**
     * Returns the gravatar url from email
     */
    function gravatar($email, $null_if_missing = FALSE) {
        $md5 = md5(strtolower($email));

        if ($null_if_missing) {
            $gravatar = "http://www.gravatar.com/avatar/$md5?d=404";
            $headers = get_headers($gravatar, 1);

            if (strpos($headers[0], '404')) {
                return NULL;
            }
        }

        return 'http://www.gravatar.com/avatar/' . $md5 . 'fs=150';
    }
}

if (!function_exists('is_disposable_email')) {
    /**
     * Checks if the email address is disposable
     */
    function is_disposable_email($email, $mx_check = TRUE) {
        $disposable = FALSE;
        $domain = strtolower(array_last(explode('@', $email)));

        if (!in_array($domain, ['gmail.com', 'hotmail.com', 'outlook.com', 'mail.com', 'yahoo.com', 'gmx.com', 'fastmail.com', 'hushmail.com', 'inbox.com'])) {
            if (!empty($mx_check) && !checkdnsrr($domain, 'MX')) {
                $disposable = TRUE;
            } else {
                $result = @json_decode(curl("https://open.kickbox.com/v1/disposable/" . urlencode($email)), TRUE);
                $disposable = (!empty($result['disposable']) && ($result['disposable'] === TRUE));
            }
        }

        return $disposable;
    }
}

if (!function_exists('blank_image')) {
    /**
     * Returns data for a blank image
     *
     * $data = blank_image('gif|png')
     */
    function blank_image($type = 'gif') {
        return decode_image_data($type === 'gif' ? 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==' :
            'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=');
    }
}

if (!function_exists('imgur')) {
    /**
     * Uploads an image to imgur and return it's online URL
     *
     * The following env vars must be defined: IMGUR_KEY
     *
     */
    function imgur($path) {
        $client_id = getenv('IMGUR_KEY') ?: die('IMGUR_KEY is missing');
        $image = file_get_contents($path);

        $output = curl('https://api.imgur.com/3/image.json', 'post', ['image' => base64_encode($image)], ['Authorization: Client-ID ' . $client_id]);
        $reply = json_decode($output);

        return $reply->data->link;
    }
}

if (!function_exists('dom_path')) {
    /**
     * Creates a DOM document and returns it's xpath selector
     *
     * $xpath = dom_path(curl('http://www.example.com')); // foreach($xpath->query('//h1') as $node) print $node->textContent;
     *
     *  For CSS selector: Use BrowserKit or pQuery!
     *  More here: https://stackoverflow.com/a/260609/1031454
     */
    function dom_path($html) {
        libxml_use_internal_errors(TRUE);

        $doc = new DOMDocument();
        $doc->loadHTML($html);
        $xpath = new DOMXPath($doc);

        return $xpath;
    }
}

if (!function_exists('download')) {
    /**
     * Downloads a file
     *
     * $file = download('http://www.bakalafoundation.org/wp/wp-content/uploads/2016/03/Google_logo_420_color_2x-1.png'[, 'c:/tmp/test.jpg']);
     *
     */
    function download($url, $file = NULL, $headers = []) {
        if ($data = curl($url, 'get', [], $headers, $status)) {
            if ($status === 200) {
                $file = $file ?? sprintf('%s.%s', tempnam(sys_get_temp_dir(), 'download'), pathinfo($url, PATHINFO_EXTENSION));
                return file_put_contents($file, $data) ? $file : FALSE;
            }
        }

        return FALSE;
    }
}

if (!function_exists('email')) {
    /**
     * Send out an email using Amazon SES
     *
     * The following keys must be defined: AWS_SECRET_ACCESS_KEY, AWS_ACCESS_KEY_ID
     *
     */
    //function email($to, $subject, $body, $attach = NULL) {
    function email($to, $from, $subject, $html, $text = FALSE, $attachment = NULL, $headers = []) {
        $date = date('r');
        $from = $from ?: getenv('MAIL_FROM') ?: sprintf('"Support" <support@%s>', getenv('APP_DOMAIN') ?: 'localhost');
        $priv_key = getenv('AWS_SECRET_ACCESS_KEY') ?: die('aws secret key not present');
        $access_key = getenv('AWS_ACCESS_KEY_ID') ?: die('aws access id not present');
        $signature = base64_encode(hash_hmac('sha256', $date, $priv_key, TRUE));
        $auth_header = "X-Amzn-Authorization: AWS3-HTTPS AWSAccessKeyId=$access_key, Algorithm=HmacSHA256, Signature=$signature";
        $endpoint = "https://email.us-east-1.amazonaws.com/";

        if (!empty($html) && empty($text)) {
            $text = $text === FALSE ? '' : html2text($html);
        } elseif (empty($html) && !empty($text)) {
            $html = nl2br($text);
        }

        $random_hash = 'site_' . md5(date('r', time()));
        $emailText = "Return-Path: {$from}" . "\r\n";
        $emailText .= "Reply-To: $from" . "\r\n";
        $emailText .= "From: {$from}" . "\r\n";
        $emailText .= 'To: ' . $to . "\r\n";
        $emailText .= "Subject: {$subject}" . "\r\n";
        $emailText .= 'Date: ' . date('D, j M Y H:i:s O') . "\r\n";

        foreach ($headers as $key => $value) {
            $emailText = preg_replace("/$key\:.*\n/", '', $emailText);
            $emailText .= "$key: $value\r\n";
        }

        $emailText .= "Content-Type: multipart/alternative; " . "\r\n";
        $emailText .= "\t" . 'boundary="' . $random_hash . '"' . "\r\n";
        $emailText .= 'MIME-Version: 1.0' . "\r\n\r\n";

        $emailText .= '--' . $random_hash . "\r\n";
        $emailText .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";
        $emailText .= 'Content-Transfer-Encoding: quoted-printable' . "\r\n\r\n\r\n";
        $html = preg_replace("#(?<!\r)\n#si", "\r\n", $html) . "\r\n";
        $emailText .= quoted_printable_encode($html) . "\r\n";

        if (!empty($text)) {
            $emailText .= '--' . $random_hash . "\r\n";
            $emailText .= 'Content-Type: text/plain; charset=us-ascii' . "\r\n";
            $emailText .= 'Content-Transfer-Encoding: 7bit' . "\r\n\r\n\r\n";
            $emailText .= $text . "\r\n\r\n\r\n";
        }

        if (!empty($attachment)) {
            list($file, $name) = is_string($attachment) ? [$attachment, basename($attachment)] : [$attachment['file'], $attachment['name']];

            $content = file_get_contents($file);
            $content = chunk_split(base64_encode($content));

            $emailText .= '--' . $random_hash . "\r\n";
            $emailText .= "Content-Type: application/octet-stream; name=\"" . $name . "\"" . "\r\n";
            $emailText .= "Content-Transfer-Encoding: base64" . "\r\n";
            $emailText .= "Content-Disposition: attachment" . "\r\n\r\n\r\n";
            $emailText .= $content . "\r\n";
        }

        $emailText .= '--' . $random_hash . "--";

        if ($fn = getenv('MAIL_DEBUG_ONLY')) {
            return debug(write_file($fn != 'true' ? $fn : temp_file('txt'), $emailText));
        }

        $params = [
            'Action'                => 'SendRawEmail',
            'Source'                => $from,
            'Destinations.member.1' => $to,
            "Version"               => "2010-12-01",
            'RawMessage.Data'       => base64_encode($emailText),
        ];

        $output = curl($endpoint, 'post', $params, [$auth_header, "Date: $date"], $status);

        if ($status !== 200) {
            debug($output);
        }

        return $status === 200;
    }
}

if (!function_exists('domain')) {
    /**
     * Gets the domain from URL with or without sub-domain
     *
     */
    function domain($url, $tld = TRUE) {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];

        if (empty($tld)) {
            return $domain;
        } else if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }

        return FALSE;
    }
}

if (!function_exists('fake_data')) {
    /**
     * return fake user data from uinames.com
     *
     * $user = fake_data();
     *
     */
    function fake_data() {
        return @json_decode(curl('https://uinames.com/api/?ext&region=united+states'), TRUE);
    }
}

# Utility functions

if (!function_exists('today')) {
    /**
     * ## Other functions
     *
     * Return's today's date in dd-mm-yyyy format (utc)
     *
     * $v = today('dd/mm/yy')
     */
    function today($format = 'dd-mm-yyyy', $time = NULL) {
        $map = ['dd' => 'd', 'mm' => 'm', 'yyyy' => 'Y', 'yy' => 'y', 't' => 'h:i:s', 'z' => 'T'];

        return date(strtr($format, $map), $time ?? time());
    }
}

if (!function_exists('elapsed')) {
    /**
     * Show number of seconds elapsed after $time
     */
    function elapsed($time, $now = NULL) {
        $time = strtotime($time);
        $now = !empty($now) ? strtotime($now) : time();

        return $now - $time;
    }
}

if (!function_exists('mysql_date')) {
    /**
     * Coverts any date to Mysql format
     */
    function mysql_date($date = NULL) {
        $time = !empty($date) ? (is_numeric($date) ? $date : strtotime($date)) : time();
        return date('Y-m-d h:i:s', $time);
    }
}

if (!function_exists('is_debug')) {
    /**
     * Checks if debugging is enabled, i.e. global $debug var is set or DEBUG env is present
     */
    function is_debug() {
        global $debug;
        return $debug ?? @((getenv('APP_DEBUG') == 'true') ?: (getenv('DEBUG') == 'true'));
    }
}

if (!function_exists('debug')) {
    /**
     * Prints to STDERR
     */
    function debug(...$args) {
        if (is_debug()) {
            $msg = sprintf('%s: %s', mysql_date(), join("\n", $args));
            file_put_contents('php://stderr', $msg . PHP_EOL, FILE_APPEND);
            return TRUE;
        }

        return FALSE;
    }
}

if (!function_exists('warning')) {
    /**
     * Prints to STDERR with red color
     */
    function warning(...$args) {
        $msg = sprintf('%s: %s', mysql_date(time()), join("\n", $args));
        debug("\e[1;30m\e[41m$msg\e[0m");
    }
}


if (!function_exists('app_dir')) {
    /**
     * Return app's base dir
     */
    function app_dir($path = NULL, $check_exists = TRUE) {
        $base = is_cli() ? getcwd() : rtrim(realpath($_SERVER['DOCUMENT_ROOT'] . '/../'), '/');
        return !empty($path) ? ($check_exists ? realpath("$base/$path") : "$base/$path") : $base;
    }
}

if (!function_exists('log_to_file')) {
    /**
     * Write a line to log file
     */
    function log_to_file($file, ...$args) {
        $logDir = app_dir('logs');
        if (!is_dir($logDir)) mkdir($logDir);
        $path = sprintf("%s/%s.log", $logDir, preg_replace('/(\.log)$/i', '', $file));
        $log = sprintf('%s: %s', mysql_date(), single_line(var_export($args)));

        return file_put_contents($path, $log . PHP_EOL, FILE_APPEND);
    }
}

if (!function_exists('log_error')) {
    /**
     * Logs error in ~/error.log
     */
    function log_error(...$args) {
        return call_user_func_array('log_to_file', array_merge(['error.log'], $args));
    }
}

if (!function_exists('ip_address')) {
    /**
     * Get's the local IP address
     */
    function ip_address() {
        $ip = getenv('HTTP_CLIENT_IP') ?: getenv('HTTP_X_FORWARDED_FOR') ?: getenv('HTTP_X_FORWARDED') ?: getenv('HTTP_FORWARDED_FOR') ?: getenv('HTTP_FORWARDED') ?: getenv('REMOTE_ADDR');
        return !empty($ip) && $ip !== '127.0.0.1' ? $ip : '63.70.164.200';
    }
}

if (!function_exists('env')) {
    /**
     * Return value of an environment variable
     */
    function env($name, $default = NULL) {
        return getenv($name) ?: ($_ENV[$name] ?? $default);
    }
}

if (!function_exists('os')) {
    /**
     * Return the OS name (windows, mac, or linux)
     */
    function os() {
        $uname = strtolower(php_uname());
        return (strpos($uname, "darwin") !== FALSE) ? 'mac' : ((strpos($uname, "win") !== FALSE) ? 'windows' : 'linux');
    }
}

if (!function_exists('cmd')) {
    /**
     * Runs a command using `system`
     * - If any arg starts with a @ (or is not a scalar), it is saved in tmpfile and the file path is used instead
     * - If first arg is "false" then the command is dry-run
     *
     * cmd('curl -F "data=@%s" -F "userid=%d" http://example.com', '@{"json":"data"}', 3); will automatically save first argument to file and insert its path
     */
    function cmd(...$args) {
        set_time_limit(0);

        if ($args[0] === FALSE) {
            $test = TRUE;
            array_splice($args, 0, 1);
        }

        for ($i = 0; $i < count($args); $i++) {
            $arg = $args[$i];

            if (!is_scalar($arg)) {
                $args[$i] = unix_path(write_file(temp_file('json'), $arg));
            } elseif (strpos($arg, '@') === 0) {
                $args[$i] = unix_path(write_file(temp_file('json'), substr($arg, 1)));
            }
        }

        $cmd = call_user_func_array('sprintf', $args);

        debug("CMD: " . $cmd);

        return !empty($test) ? $cmd : system($cmd);
    }
}

if (!function_exists('bg')) {
    /**
     * Runs a process in the background
     */
    function bg(...$args) {
        set_time_limit(0);

        $os = os();
        $cmd = call_user_func_array('sprintf', $args);
        $cmd1 = sprintf("%s %s %s", $os == 'windows' ? 'start ""' : ($os == 'mac' ? 'open' : 'xdg-open'), $cmd, $os == 'windows' ? '' : '&');

        pclose(popen($cmd1, "r"));
    }
}

if (!function_exists('cmd_capture')) {
    /**
     * Captures the output of command using backticks and trims the output
     */
    function cmd_capture(...$args) {
        set_time_limit(0);
        $cmd = call_user_func_array('sprintf', $args);
        $cmd = is_debug() ? sprintf('%s', $cmd) : $cmd;
        debug("CMD: ", $cmd);
        return trim(`$cmd`);
    }
}

if (!function_exists('cmd_parallel')) {
    /**
     * Runs commands in parallel using xargs
     *
     * $o = cmd_parallel(['youtube-dl 1', 'youtube-dl 2', 'php sleep.php'], 3);
     */
    function cmd_parallel(array $commands, int $max_parallel = 3) {
        $fn = temp_file('.tmp');
        $str = join("\n", array_map(function ($f) { return sprintf('"%s"', strtr($f, ['"' => '\\"'])); }, $commands));
        file_put_contents($fn, $str);

        return cmd('xargs -n 1 -P %d %s < %s', $max_parallel, os() === 'windows' ? 'cmd /c' : 'sh -c', $fn);
    }
}

if (!function_exists('cache_memory')) {
    /**
     * Caches (read/write) value to memory
     *
     * $r = cache_memory('key', [1, 2]);
     */
    function cache_memory($key, $value = NULL) {
        if (empty($GLOBALS['mycache']) && !empty($value)) {
            $value = $value instanceof \Closure ? $value() : $value;
            $GLOBALS['mycache'] = $value;
        }

        return $GLOBALS['mycache'] ?? NULL;
    }
}

if (!function_exists('cache_local')) {
    /**
     * Caches (read/write) value to file cache
     *
     * $r = cache_local('key', [1, 2], -86400, true); //fresh result - cache for 86400
     */
    function cache_local($key, $value = NULL, $expires = 86400, $refresh = FALSE) {
        $file = sprintf('%s/cache-%s.php', sys_get_temp_dir(), $key);

        if (file_exists($file) && !$refresh) {
            debug("cache: $file");

            try {
                if ($cached = @include($file)) {
                    return $cached;
                }
            } catch (\Throwable $e) {
                debug("Error: " . $e->getMessage());
            }
        }

        $value = $value instanceof \Closure ? $value() : $value;
        $script = sprintf('<?' . "php return time() > %d ? null : json_decode_array('%s');", time() + abs($expires), str_replace("'", "\'", json_encode($value)));

        return file_put_contents($file, $script) ? $value : FALSE;
    }
}

if (!function_exists('confirm')) {
    /**
     * Waits for confirmation from command line
     *
     */
    function confirm($msg = 'Are you sure?', $default = 'y', $accept = '/^y/i') {
        echo $msg, $default ? " ($default)" : '', "\n";

        $handle = fopen("php://stdin", "r");
        $line = fgets($handle);
        $yes = preg_match($accept, trim($line) ?: $default);
        fclose($handle);

        return $yes;
    }
}

if (!function_exists('dump')) {
    /**
     * Dumps variables to screen or browser (with proper formatting)
     */
    function dump(...$args) {
        $dump = json_encode($args, JSON_PRETTY_PRINT);

        if (is_cli()) {
            echo $dump, "\n";
        } else {
            echo "\n<pre><code>$dump</code></pre>\n";
        }
    }
}

if (!function_exists('encrypt_str')) {
    /**
     * Encrypts a string
     */
    function encrypt_str($string, $password = '') {
        $password = ($password ?: getenv('APP_SECRET')) ?: die('password is required');

        return openssl_encrypt($string, "AES-128-ECB", $password);
    }
}

if (!function_exists('decrypt_str')) {
    /**
     * Decrypts a string encoded with encrypt
     */
    function decrypt_str($string, $password = '') {
        $password = ($password ?: getenv('APP_SECRET')) ?: die('password is required');

        return openssl_decrypt($string, "AES-128-ECB", $password);
    }
}

# CLI Related

if (!function_exists('is_cli')) {
    /**
     * Checks if PHP is running in CLI mode
     */
    function is_cli() {
        return php_sapi_name() === "cli";
    }
}

if (!function_exists('get_argv')) {
    /**
     * Gets an argument by name (or alias) from command line
     *
     * $val = get_argv('name', null);
     *
     * @return mixed
     */
    function get_argv($name = NULL, $default = NULL) {
        global $argv;
        $args = @array_map('trim', array_map_args('ltrim', preg_split('/\s+\-/', join(' ', array_slice($argv, 1))), '-'));
        foreach ($args as $arg) {
            @list($key, $value) = preg_split('/\s+/', $arg, 2);
            $opts[$key] = isset($opts[$key]) ? array_merge((array) $opts[$key], [$value]) : ($value ?: TRUE);
        }

        return empty($name) ? ($opts ?? []) : (@$opts[$name] ?: $default);
    }
}

if (!function_exists('show_errors')) {
    /**
     * Shows or hides all errors
     */
    function show_errors($show = TRUE) {
        ini_set('display_errors', $show ? 1 : 0);
        ini_set('display_startup_errors', $show ? 1 : 0);
        error_reporting($show ? E_ALL : E_ERROR);
        putenv('DEBUG' . $show ? '=true' : '');
    }
}

